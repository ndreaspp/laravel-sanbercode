<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function Register () {
        return view ('data.register');
    }
    public function Welcome(Request $request) {
        $firstname = $request->input("fname");
        $lastname = $request->input("lname");

        // dd($firstname);
        
        return view ('data.welcome', ['FName'=>$firstname,'LName'=>$lastname]);
    }
}
